#!/usr/bin/env python
# coding: utf-8

# ## Data Understanding

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

loan = pd.read_csv("loan.csv", sep=",")
loan.info()


# In[2]:


loan.head()


# In[3]:


loan.columns


# ## Data Cleaning

# In[4]:


# summarising number of missing values in each column
loan.isnull().sum()


# In[5]:


# percentage of missing values in each column
round(loan.isnull().sum()/len(loan.index), 2)*100


# In[6]:


# removing the columns having more than 90% missing values
missing_columns = loan.columns[100*(loan.isnull().sum()/len(loan.index)) > 90]
print(missing_columns)


# In[7]:


loan = loan.drop(missing_columns, axis=1)
print(loan.shape)


# In[8]:


# summarise number of missing values again
100*(loan.isnull().sum()/len(loan.index))


# In[9]:


# let's have a look at desc and math-since.. in the columns
loan.loc[:, ['desc', 'mths_since_last_delinq']].head()


# In[10]:


# dropping the two columns
loan = loan.drop(['desc', 'mths_since_last_delinq'], axis=1)


# In[11]:


# summarise number of missing values again
100*(loan.isnull().sum()/len(loan.index))


# In[12]:


# missing values in rows
loan.isnull().sum(axis=1)


# In[13]:


# checking whether some rows have more than 5 missing values
len(loan[loan.isnull().sum(axis=1) > 5].index)


# In[14]:


loan.info()


# In[15]:


# The column int_rate is character type, let's convert it to float
loan['int_rate'] = loan['int_rate'].apply(lambda x: pd.to_numeric(x.split("%")[0]))


# In[16]:


# checking the data types
loan.info()


# In[19]:


# drop missing values from emp_lenght column
loan = loan[~loan['emp_length'].isnull()]

# using regular expression to extract numeric values from the string
import re
loan['emp_length'] = loan['emp_length'].apply(lambda x: re.findall('\d+', str(x))[0])

# convert to numeric
loan['emp_length'] = loan['emp_length'].apply(lambda x: pd.to_numeric(x))


# In[20]:


# looking at type of the columns again
loan.info()


# ## Data Analysis

# In[21]:


behaviour_var =  [
  "delinq_2yrs",
  "earliest_cr_line",
  "inq_last_6mths",
  "open_acc",
  "pub_rec",
  "revol_bal",
  "revol_util",
  "total_acc",
  "out_prncp",
  "out_prncp_inv",
  "total_pymnt",
  "total_pymnt_inv",
  "total_rec_prncp",
  "total_rec_int",
  "total_rec_late_fee",
  "recoveries",
  "collection_recovery_fee",
  "last_pymnt_d",
  "last_pymnt_amnt",
  "last_credit_pull_d",
  "application_type"]
behaviour_var


# In[22]:


# let's now remove the behaviour variables from analysis
df = loan.drop(behaviour_var, axis=1)
df.info()


# In[23]:


# remove some useless 
df = df.drop(['title', 'url', 'zip_code', 'addr_state'], axis=1)


# In[24]:


df['loan_status'] = df['loan_status'].astype('category')
df['loan_status'].value_counts()


# In[25]:


# filtering only fully paid or charged-off
df = df[df['loan_status'] != 'Current']
df['loan_status'] = df['loan_status'].apply(lambda x: 0 if x=='Fully Paid' else 1)

# converting loan_status to integer type
df['loan_status'] = df['loan_status'].apply(lambda x: pd.to_numeric(x))

# summarising the values
df['loan_status'].value_counts()


# ### Univariate Analysis

# In[26]:


# default rate
round(np.mean(df['loan_status']), 2)


# In[27]:


# plotting default rates across grade of the loan
sns.barplot(x='grade', y='loan_status', data=df)
plt.show()


# In[28]:


# lets define a function to plot loan_status across categorical variables
def plot_cat(cat_var):
    sns.barplot(x=cat_var, y='loan_status', data=df)
    plt.show()


# In[29]:


# compare default rates across grade of loan
plot_cat('grade')


# In[30]:


# term: 60 months loans default more than 36 months loans
plot_cat('term')


# In[31]:


# sub-grade: as expected - A1 is better than A2 better than A3 and so on 
plt.figure(figsize=(16, 6))
plot_cat('sub_grade')


# In[32]:


# home ownership: not a great discriminator
plot_cat('home_ownership')


# In[33]:


# verification_status: surprisingly, verified loans default more than not verifiedb
plot_cat('verification_status')


# In[34]:


# purpose: small business loans defualt the most, then renewable energy and education
plt.figure(figsize=(16, 6))
plot_cat('purpose')


# In[35]:


# let's also observe the distribution of loans across years
# first lets convert the year column into datetime and then extract year and month from it
df['issue_d'].head()


# In[36]:


from datetime import datetime
df['issue_d'] = df['issue_d'].apply(lambda x: datetime.strptime(x, '%b-%y'))


# In[37]:


# extracting month and year from issue_date
df['month'] = df['issue_d'].apply(lambda x: x.month)
df['year'] = df['issue_d'].apply(lambda x: x.year)


# In[38]:


# let's first observe the number of loans granted across years
df.groupby('year').year.count()


# In[39]:


# number of loans across months
df.groupby('month').month.count()


# In[40]:


# lets compare the default rates across years
# the default rate had suddenly increased in 2011, inspite of reducing from 2008 till 2010
plot_cat('year')


# In[41]:


# comparing default rates across months: not much variation across months
plt.figure(figsize=(16, 6))
plot_cat('month')


# In[42]:


# loan amount: the median loan amount is around 10,000
sns.distplot(df['loan_amnt'])
plt.show()


# In[43]:


# binning loan amount
def loan_amount(n):
    if n < 5000:
        return 'low'
    elif n >=5000 and n < 15000:
        return 'medium'
    elif n >= 15000 and n < 25000:
        return 'high'
    else:
        return 'very high'
        
df['loan_amnt'] = df['loan_amnt'].apply(lambda x: loan_amount(x))


# In[44]:


df['loan_amnt'].value_counts()


# In[45]:


# let's compare the default rates across loan amount type
# higher the loan amount, higher the default rate
plot_cat('loan_amnt')


# In[46]:


# let's also convert funded amount invested to bins
df['funded_amnt_inv'] = df['funded_amnt_inv'].apply(lambda x: loan_amount(x))


# In[47]:


# funded amount invested
plot_cat('funded_amnt_inv')


# In[48]:


# lets also convert interest rate to low, medium, high
# binning loan amount
def int_rate(n):
    if n <= 10:
        return 'low'
    elif n > 10 and n <=15:
        return 'medium'
    else:
        return 'high'
    
    
df['int_rate'] = df['int_rate'].apply(lambda x: int_rate(x))


# In[49]:


# comparing default rates across rates of interest
# high interest rates default more, as expected
plot_cat('int_rate')


# In[50]:


# debt to income ratio
def dti(n):
    if n <= 10:
        return 'low'
    elif n > 10 and n <=20:
        return 'medium'
    else:
        return 'high'
    

df['dti'] = df['dti'].apply(lambda x: dti(x))


# In[51]:


# comparing default rates across debt to income ratio
# high dti translates into higher default rates, as expected
plot_cat('dti')


# In[52]:


# funded amount
def funded_amount(n):
    if n <= 5000:
        return 'low'
    elif n > 5000 and n <=15000:
        return 'medium'
    else:
        return 'high'
    
df['funded_amnt'] = df['funded_amnt'].apply(lambda x: funded_amount(x))


# In[53]:


plot_cat('funded_amnt')


# In[54]:


# installment
def installment(n):
    if n <= 200:
        return 'low'
    elif n > 200 and n <=400:
        return 'medium'
    elif n > 400 and n <=600:
        return 'high'
    else:
        return 'very high'
    
df['installment'] = df['installment'].apply(lambda x: installment(x))


# In[55]:


# comparing default rates across installment
# the higher the installment amount, the higher the default rate
plot_cat('installment')


# In[56]:


# annual income
def annual_income(n):
    if n <= 50000:
        return 'low'
    elif n > 50000 and n <=100000:
        return 'medium'
    elif n > 100000 and n <=150000:
        return 'high'
    else:
        return 'very high'

df['annual_inc'] = df['annual_inc'].apply(lambda x: annual_income(x))


# In[57]:


# annual income and default rate
# lower the annual income, higher the default rate
plot_cat('annual_inc')


# In[58]:


# employment length
# first, let's drop the missing value observations in emp length
df = df[~df['emp_length'].isnull()]

# binning the variable
def emp_length(n):
    if n <= 1:
        return 'fresher'
    elif n > 1 and n <=3:
        return 'junior'
    elif n > 3 and n <=7:
        return 'senior'
    else:
        return 'expert'

df['emp_length'] = df['emp_length'].apply(lambda x: emp_length(x))


# In[59]:


# emp_length and default rate
# not much of a predictor of default
plot_cat('emp_length')


# ### Segmented Univariate Analysis

# In[60]:


# purpose: small business loans defualt the most, then renewable energy and education
plt.figure(figsize=(16, 6))
plot_cat('purpose')


# In[61]:


# lets first look at the number of loans for each type (purpose) of the loan
# most loans are debt consolidation (to repay otehr debts), then credit card, major purchase etc.
plt.figure(figsize=(16, 6))
sns.countplot(x='purpose', data=df)
plt.show()


# In[62]:


# filtering the df for the 4 types of loans mentioned above
main_purposes = ["credit_card","debt_consolidation","home_improvement","major_purchase"]
df = df[df['purpose'].isin(main_purposes)]
df['purpose'].value_counts()


# In[63]:


# plotting number of loans by purpose 
sns.countplot(x=df['purpose'])
plt.show()


# In[64]:


# let's now compare the default rates across two types of categorical variables
# purpose of loan (constant) and another categorical variable (which changes)

plt.figure(figsize=[10, 6])
sns.barplot(x='term', y="loan_status", hue='purpose', data=df)
plt.show()


# In[65]:


# lets write a function which takes a categorical variable and plots the default rate
# segmented by purpose 

def plot_segmented(cat_var):
    plt.figure(figsize=(10, 6))
    sns.barplot(x=cat_var, y='loan_status', hue='purpose', data=df)
    plt.show()

    
plot_segmented('term')


# In[66]:


# grade of loan
plot_segmented('grade')


# In[67]:


# home ownership
plot_segmented('home_ownership')


# In[68]:


# year
plot_segmented('year')


# In[69]:


# emp_length
plot_segmented('emp_length')


# In[70]:


# loan_amnt: same trend across loan purposes
plot_segmented('loan_amnt')


# In[71]:


# interest rate
plot_segmented('int_rate')


# In[72]:


# installment
plot_segmented('installment')


# In[73]:


# debt to income ratio
plot_segmented('dti')


# In[74]:


# annual income
plot_segmented('annual_inc')


# In[75]:


# variation of default rate across annual_inc
df.groupby('annual_inc').loan_status.mean().sort_values(ascending=False)


# In[76]:


# one can write a function which takes in a categorical variable and computed the average 
# default rate across the categories
# It can also compute the 'difference between the highest and the lowest default rate' across the 
# categories, which is a decent metric indicating the effect of the varaible on default rate

def diff_rate(cat_var):
    default_rates = df.groupby(cat_var).loan_status.mean().sort_values(ascending=False)
    return (round(default_rates, 2), round(default_rates[0] - default_rates[-1], 2))

default_rates, diff = diff_rate('annual_inc')
print(default_rates) 
print(diff)


# In[77]:


# filtering all the object type variables
df_categorical = df.loc[:, df.dtypes == object]
df_categorical['loan_status'] = df['loan_status']

# Now, for each variable, we can compute the incremental diff in default rates
print([i for i in df.columns])


# In[78]:


# storing the diff of default rates for each column in a dict
d = {key: diff_rate(key)[1]*100 for key in df_categorical.columns if key != 'loan_status'}
print(d)


# In[ ]:




